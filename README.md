# alfred-gitlab-file-finder

A tiny [Alfred](https://www.alfredapp.com/) workflow to search for files in GitLab repository.

![Demo](./assets/doc/demo.gif)

This workflow: 
- Uses [Alfred-Workflow python library](https://github.com/deanishe/alfred-workflow) to do most of the work
- Caches http response for 10 minutes
- Fuzzy filters the response  
- Opens selected file in default browser

## Installation

Download latest `alfred-gitlab-file-finder-*.*.alfredworkflow` file from [releases page](https://gitlab.com/georgekoltsov/alfred-gitlab-file-finder/-/releases) & double click it.
