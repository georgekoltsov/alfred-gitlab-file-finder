#!/usr/bin/python
# encoding: utf-8

import sys
from workflow import Workflow, ICON_WEB, web

def get_gitlab_files():
    url = 'https://gitlab.com/gitlab-org/gitlab/-/files/master?format=json'

    r = web.get(url)
    r.raise_for_status()

    return r.json()

def search_key_for_file(file):
    return file

def main(wf):
    if len(wf.args):
        query = wf.args[0]
    else:
        query = None

    files = wf.cached_data('files', get_gitlab_files, max_age=600)

    if query:
        files = wf.filter(query, files, key=search_key_for_file)

    for file in files:
        wf.add_item(title=file,
                    arg='https://gitlab.com/gitlab-org/gitlab/-/blob/master/' + file,
                    valid=True,
                    icon=ICON_WEB)

    wf.send_feedback()


if __name__ == u"__main__":
    wf = Workflow()
    sys.exit(wf.run(main))
